<div align="center">
   <h1 style="text-align: center">🖖🏽&nbsp;&nbsp;Olá galerinha! Eu sou Ciro Mota</h1>
</div>

   - 🏠 Resido em Salvador, Bahia.
   - 💻 Atualmente estudando Desenvolvimento Frontend.
   - 💖 Procuro sempre formas de desenvolver e ajudar o Open Source.

  ---

<div align="center">
    <p style="text-align:left;text-indent:30px font-size:16px"><samp>Tecnólogo por formação. Pequeno criador de conteúdo nas horas vagas. Nerd, um pouco gamer. Ávido por desafios e por um constante aprendizado. Tropecei no mundo FLOSS e fascinei. Desde então me considero um defensor desse mundo.</samp></p>
</div>

<div align="center">
  <h2 style="text-align: center">📫 Me contate</h2>
</div>

<div align="center">
  <a href="https://www.linkedin.com/in/ciro-mota/" target="blank"><img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/linked-in-alt.svg" alt="Siga-me no LinkedIn!" height="30" width="40" /></a> 
  <a href="https://twitter.com/ciromota" target="blank"><img src="https://raw.githubusercontent.com/rahuldkjain/github-profile-readme-generator/master/src/images/icons/Social/twitter.svg" alt="Siga-me no Twitter!" height="30" width="40" /></a> 
</div>

<div align="center">
  <h2 style="text-align: center">🛠️ Contribuições</h2>
</div>

<div align="center">
  <img src="https://github-readme-stats-git-masterrstaa-rickstaa.vercel.app/api?username=ciro-mota&hide=commits,prs,issues&show_icons=true&hide_rank=true&theme=default#gh-light-mode-only" alt="My GitHub Stats" />
  </br>
  </br>
  <p style="font-size:16px;font-weight:bold;">&#8226; <a href="https://gitlab.gnome.org/raggesilver/blackbox">Black Box</a> &nbsp;&nbsp; <img src="https://img.shields.io/gitlab/stars/raggesilver/blackbox?gitlab_url=https%3A%2F%2Fgitlab.gnome.org&style=social"></p>

<p style="font-size:16px;font-weight:bold;">&#8226; <a href="https://gitlab.com/volian/nala">Nala</a> &nbsp;&nbsp; <img src="https://img.shields.io/gitlab/stars/volian/nala?gitlab_url=https%3A%2F%2Fgitlab.com&style=social"></p>
</div>

 <div align="center">
    <h2 style="text-align: center">💻 Tecnologias que eu uso no meu dia</h2>
 </div>

<div style="display: inline_block" align="center">
  <img alt="Linux" src="https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black" /> 
  <img alt="Arch Linux" src="https://img.shields.io/badge/Arch_Linux-1793D1?style=for-the-badge&logo=arch-linux&logoColor=white" /> 
  <img alt="OpenWrt" src="https://img.shields.io/badge/OpenWrt-00B5E2?style=for-the-badge&logo=OpenWrt&logoColor=white" /> 
  <img alt="VSCodium" src="https://img.shields.io/badge/VSCodium-0078d7.svg?style=for-the-badge&logo=visual-studio-code&logoColor=white"/> 
  <img alt="Markdown" src="https://img.shields.io/badge/Markdown-000000?style=for-the-badge&logo=markdown&logoColor=white" /> 
  <img alt="Shell Script" src="https://img.shields.io/badge/shell_script-%23121011.svg?style=for-the-badge&logo=gnu-bash&logoColor=white"/> 
  <img alt="Git" src="https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white"/> 
  <img alt="GitHub" src="https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white"/> 
  <img alt="Gitlab" src="https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white" />
  <img alt="Docker" src="https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white"/>
  <img alt="Terraform" src="https://img.shields.io/badge/terraform-%235835CC.svg?style=for-the-badge&logo=terraform&logoColor=white" />
</div>